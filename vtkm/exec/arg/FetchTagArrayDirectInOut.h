//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2015 Sandia Corporation.
//  Copyright 2015 UT-Battelle, LLC.
//  Copyright 2015 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_exec_arg_FetchTagArrayDirectInOut_h
#define vtk_m_exec_arg_FetchTagArrayDirectInOut_h

#include <vtkm/cont/internal/IteratorFromArrayPortal.h>
#include <vtkm/exec/arg/AspectTagDefault.h>
#include <vtkm/exec/arg/Fetch.h>
#include <vtkm/exec/arg/BlockFetch.h>
#include <algorithm> //for copy_n
#include <vtkm/VecTraits.h>

namespace vtkm
{
namespace exec
{
namespace arg
{

/// \brief \c Fetch tag for in-place modifying array values with direct indexing.
///
/// \c FetchTagArrayDirectInOut is a tag used with the \c Fetch class to do
/// in-place modification of values in an array portal. The fetch uses direct
/// indexing, so the thread index given to \c Store is used as the index into
/// the array.
///
/// When using \c FetchTagArrayDirectInOut with a worklet invocation with a
/// scatter, it is a bit undefined how the in/out array should be indexed.
/// Should it be the size of the input arrays and written back there, or
/// should it be the size of the output arrays and pre-filled with the output.
/// The implementation indexes based on the output because it is safer. The
/// output will have a unique index for each worklet instance, so you don't
/// have to worry about writes stomping on each other (which they would
/// inevitably do if index as input).
///
struct FetchTagArrayDirectInOut
{
};

template <typename ThreadIndicesType, typename ExecObjectType>
struct Fetch<vtkm::exec::arg::FetchTagArrayDirectInOut,
             vtkm::exec::arg::AspectTagDefault,
             ThreadIndicesType,
             ExecObjectType>
{
  using ValueType = typename ExecObjectType::ValueType;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices, const ExecObjectType& arrayPortal) const
  {
    return arrayPortal.Get(indices.GetCurrentOutputIndex());
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  void Store(const ThreadIndicesType& indices,
             const ExecObjectType& arrayPortal,
             const ValueType& value) const
  {
    arrayPortal.Set(indices.GetCurrentOutputIndex(), value);
  }
};

struct BlockFetchTagArrayDirectInOut
{
};

namespace detail
{

template <typename FetchTag, typename AspectTag, typename ThreadIndicesType, typename ExecObjectType, typename IsStaticTag>
struct BlockFetchImpl;

//This specialization assumes a static vector size and can therefore use NUM_COMPONENTS
template <typename ThreadIndicesType, typename ExecObjectType>
struct BlockFetchImpl<vtkm::exec::arg::FetchTagArrayDirectInOut,
                      vtkm::exec::arg::AspectTagDefault,
                      ThreadIndicesType,
                      ExecObjectType,
                      vtkm::VecTraitsTagSizeStatic()>
{
  using ValueType = typename vtkm::exec::arg::Fetch<
      vtkm::exec::arg::FetchTagArrayDirectInOut,
      vtkm::exec::arg::AspectTagDefault,
      ThreadIndicesType,
      ExecObjectType>::ValueType;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices,
                 const ExecObjectType& arrayPortal,
                 const ValueType (&buffer)[VTKM_BLOCK]) const
  {
    ValueType value;
    for(int j = 0; j < vtkm::VecTraits<ValueType>::NUM_COMPONENTS; j++)
      {
      vtkm::VecTraits<ValueType>::SetComponent(value, j, vtkm::VecTraits<ValueType>::GetComponent(buffer[indices.GetCurrentInputIndex() - indices.GetInputIndex()],j));
      }
    return value;
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  void BlockLoad(      ThreadIndicesType& indices,
                 const ExecObjectType& arrayPortal,
                       ValueType (&buffer)[VTKM_BLOCK]) const
  {
    const typename vtkm::cont::internal::IteratorFromArrayPortal<ExecObjectType>
      iter(arrayPortal, indices.GetInputIndex());
    std::copy_n(iter, indices.GetInputIndexRange(), buffer);
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  void Store(const ThreadIndicesType& indices,
             const ExecObjectType& arrayPortal,
             const ValueType& value,
                   ValueType (&buffer)[VTKM_BLOCK]) const
  {
    for(int j = 0; j < vtkm::VecTraits<ValueType>::NUM_COMPONENTS; j++)
      {
      vtkm::VecTraits<ValueType>::SetComponent(buffer[indices.GetCurrentInputIndex()-indices.GetInputIndex()],j,vtkm::VecTraits<ValueType>::GetComponent(value,j));
      }
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  void BlockStore(      ThreadIndicesType& indices,
                  const ExecObjectType& arrayPortal,
                  const ValueType (&buffer)[VTKM_BLOCK]) const
  {
    const typename vtkm::cont::internal::IteratorFromArrayPortal<ExecObjectType>
      iter(arrayPortal, indices.GetOutputIndex());
    std::copy_n(buffer, indices.GetOutputIndexRange(), iter);
  }

  VTKM_EXEC
  constexpr vtkm::Id GetBufferSize() const
  {
    return VTKM_BLOCK;
  }
};
//This specialization assumes a variable vector size and does not use NUM_COMPONENTS
template <typename ThreadIndicesType, typename ExecObjectType>
struct BlockFetchImpl<vtkm::exec::arg::FetchTagArrayDirectInOut,
                      vtkm::exec::arg::AspectTagDefault,
                      ThreadIndicesType,
                      ExecObjectType,
                      vtkm::VecTraitsTagSizeVariable()>
{
  typedef typename ExecObjectType::ValueType ValueType;

  using Traits = vtkm::VecTraits<ValueType>;
  using ComponentType = typename Traits::ComponentType;

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices,
                 const ExecObjectType& arrayPortal,
                 const void* buffer) const
  {
    return arrayPortal.Get(indices.GetCurrentInputIndex());
  }

  VTKM_EXEC
  void Store(const ThreadIndicesType& indices,
             const ExecObjectType& arrayPortal,
             const ValueType& value,
             const void* buffer) const
  {
    arrayPortal.Set(indices.GetCurrentOutputIndex(), value);
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  void BlockLoad(ThreadIndicesType& indices,
                 const ExecObjectType& arrayPortal,
                 void* buffer) const
  {
    //no-op for static implementation
  }

  VTKM_SUPPRESS_EXEC_WARNINGS
  VTKM_EXEC
  void BlockStore(const ThreadIndicesType& indices,
                  const ExecObjectType& arrayPortal,
                  const void* buffer) const
  {
    // BlockStore is a no-op for this fetch
  }

  VTKM_EXEC
  constexpr vtkm::Id GetBufferSize() const { return 0; }
};
} //namespace detail

template <typename ThreadIndicesType, typename ExecObjectType>
struct BlockFetch<vtkm::exec::arg::FetchTagArrayDirectInOut,
                  vtkm::exec::arg::AspectTagDefault,
                  ThreadIndicesType,
                  ExecObjectType> : vtkm::exec::arg::detail::BlockFetchImpl<vtkm::exec::arg::FetchTagArrayDirectInOut,
                                                                            vtkm::exec::arg::AspectTagDefault,
                                                                            ThreadIndicesType,
                                                                            ExecObjectType,
                                                                            typename vtkm::VecTraits<typename ExecObjectType::ValueType>::IsSizeStatic()>
{
};

}
}
} // namespace vtkm::exec::arg

#endif //vtk_m_exec_arg_FetchTagArrayDirectInOut_h
