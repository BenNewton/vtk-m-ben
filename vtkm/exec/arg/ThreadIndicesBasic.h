//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2015 Sandia Corporation.
//  Copyright 2015 UT-Battelle, LLC.
//  Copyright 2015 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_exec_arg_ThreadIndicesBasic_h
#define vtk_m_exec_arg_ThreadIndicesBasic_h

#include <vtkm/internal/Invocation.h>

namespace vtkm
{
namespace exec
{
namespace arg
{

/// \brief Basic container for thread indices in a worklet invocation
///
/// During the execution of a worklet function in an execution environment
/// thread, VTK-m has to manage several indices. To simplify this management
/// and to provide a single place to store them (so that they do not have to be
/// recomputed), \c WorkletInvokeFunctor creates a \c ThreadIndices object.
/// This object gets passed to \c Fetch operations to help them load data.
///
/// All \c ThreadIndices classes should implement the functions provided in
/// the \c ThreadIndicesBasic class. (It is in fact a good idea to subclass
/// it.) Other \c ThreadIndices classes may provide additional indices if
/// appropriate for the scheduling.
///
class ThreadIndicesBasic
{
public:
  VTKM_EXEC
  ThreadIndicesBasic(vtkm::Id threadIndex,
                     vtkm::Id inIndex,
                     vtkm::IdComponent visitIndex,
                     vtkm::Id globalThreadIndexOffset = 0,
                     vtkm::Id range = 1)
    : InputIndex(inIndex)
    , OutputIndex(threadIndex)
    , VisitIndex(visitIndex)
    , GlobalThreadIndexOffset(globalThreadIndexOffset)
    , InputEndIndex(inIndex + range)
    , OutputEndIndex(threadIndex + range)
    , CurrentInputIndex(inIndex)          //todo determine if we want all these values 
    , CurrentOutputIndex(threadIndex)
  {
  }

  /// \brief The index into the input domain.
  ///
  /// This index refers to the input element (array value, cell, etc.) that
  /// this thread is being invoked for. This is the typical index used during
  /// Fetch::Load.
  ///
  VTKM_EXEC
  vtkm::Id GetInputIndex(vtkm::Id offset = 0) const { return this->InputIndex + offset; }

  /// \brief The 3D index into the input domain.
  ///
  /// This index refers to the input element (array value, cell, etc.) that
  /// this thread is being invoked for. If the input domain has 2 or 3
  /// dimensional indexing, this result will preserve that. If the domain
  /// indexing is just one dimensional, the result will have the index in the
  /// first component with the remaining components set to 0.
  ///
  VTKM_EXEC
  vtkm::Id3 GetInputIndex3D(vtkm::Id offset = 0) const { return vtkm::Id3(this->GetInputIndex(offset), 0, 0); }

  VTKM_EXEC
  vtkm::Id GetInputStartIndex() { this->CurrentInputIndex = this->InputIndex; return this->InputIndex; }
  VTKM_EXEC
  vtkm::Id GetCurrentInputIndex() const { return this->CurrentInputIndex; }
  VTKM_EXEC
  vtkm::Id GetInputEndIndex() const { return this->InputEndIndex; }
  VTKM_EXEC
  vtkm::Id GetInputNextIndex() { return ++(this->CurrentInputIndex); }
  VTKM_EXEC
  vtkm::Id GetInputIndexRange() const { return (this->InputEndIndex - this->InputIndex); }

  /// \brief The index into the output domain.
  ///
  /// This index refers to the output element (array value, cell, etc.) that
  /// this thread is creating. This is the typical index used during
  /// Fetch::Store.
  ///
  VTKM_EXEC
  vtkm::Id GetOutputIndex(vtkm::Id offset = 0) const { return this->OutputIndex + offset; }

  VTKM_EXEC
  vtkm::Id GetOutputStartIndex() { this->CurrentOutputIndex = this->OutputIndex; return this->OutputIndex; }
  VTKM_EXEC
  vtkm::Id GetCurrentOutputIndex() const { return this->CurrentOutputIndex; }
  VTKM_EXEC
  vtkm::Id GetOutputEndIndex() const { return this->OutputEndIndex; }
  VTKM_EXEC
  vtkm::Id GetOutputNextIndex() { return ++(this->CurrentOutputIndex); }
  VTKM_EXEC
  vtkm::Id GetOutputIndexRange() const { return (this->OutputEndIndex - this->OutputIndex); }

  /// \brief The visit index.
  ///
  /// When multiple output indices have the same input index, they are
  /// distinguished using the visit index.
  ///
  VTKM_EXEC
  vtkm::IdComponent GetVisitIndex() const { return this->VisitIndex; }

  /// \brief The global index (for streaming).
  ///
  /// Global index (for streaming)
  VTKM_EXEC
  vtkm::Id GetGlobalIndex() const { return (this->GlobalThreadIndexOffset + this->OutputIndex); }

private:
  vtkm::Id InputIndex;
  vtkm::Id OutputIndex;
  vtkm::IdComponent VisitIndex;
  vtkm::Id GlobalThreadIndexOffset;
  vtkm::Id InputEndIndex;
  vtkm::Id InputIndexRange;
  vtkm::Id OutputEndIndex;
  vtkm::Id OutputIndexRange;
  vtkm::Id CurrentInputIndex;
  vtkm::Id CurrentOutputIndex;
};
}
}
} // namespace vtkm::exec::arg

#endif //vtk_m_exec_arg_ThreadIndicesBasic_h
