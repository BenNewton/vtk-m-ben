//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 Sandia Corporation.
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2014 Los Alamos National Security.
//
//  Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#ifndef vtk_m_exec_arg_BlockFetch_h
#define vtk_m_exec_arg_BlockFetch_h

//#include <vtkm/Types.h>
#include <vtkm/exec/arg/Fetch.h>

namespace vtkm
{
namespace exec
{
namespace arg
{

/// \brief Struct for loading and storing values in blocks (to enable vectorization).
///
/// The \c BlockFetch struct is used to BlockLoad values into a temporary buffer
/// from which individual values can be loaded by calling Load. Similarly, Store
/// stores values into a temporary buffer from which they car BlockStored to the
/// final storage location.
///
template <typename FetchTag,
          typename AspectTag,
          typename ThreadIndicesType,
          typename ExecObjectType>
struct BlockFetch
{
  /// \brief The type of value to load, store, block load, and block store.
  ///
  /// All \c BlockFetch specializations are expected to declare a type named \c
  /// ValueType that is the type of object returned from \c Load and passed to
  /// \c Store, and the type of the elements in the buffer argument in the methods.
  ///
  using ValueType = typename vtkm::exec::arg::Fetch<FetchTag,AspectTag,ThreadIndicesType,ExecObjectType>::ValueType;

  /// \brief Load data for a work instance from the temporary buffer.
  ///
  /// All \c BlockFetch specializations are expected to have a constant method named
  /// \c Load that takes a \c ThreadIndices object containing thread-local
  /// indices and an execution object and the temporary buffer, and returns
  /// the value from the buffer appropriate for the work instance. If there
  /// is no actual data to load (for example for a write-only fetch), this
  /// method can be a no-op and return any value.
  /// This base version of Load falls back to the Fetch::Load() method and loads
  /// the value directly from the execution object.
  ///
  VTKM_EXEC
  ValueType Load(const ThreadIndicesType& indices,
                 const ExecObjectType& execObject,
                 const ValueType (&buffer)[0]) const
  {
    // pass through to a regular Fetch object
    Fetch<FetchTag, AspectTag, ThreadIndicesType, ExecObjectType> fetch;
    return fetch.Load(indices, execObject);
  }

  /// \brief Load a block of data from the execution object into the temporary buffer.
  ///
  /// All \c BlockFetch specializations are expected to have method named
  /// \c BlockLoad that takes a \c ThreadIndices object containing thread-local
  /// indices, an execution object and a buffer. The mehtod then loads a block
  /// of data into the buffer.  If there is no actual data to load (for example
  /// for a write-only fetch, or here in the base version), this method can be
  /// a no-op.
  ///
  //VTKM_SUPPRESS_EXEC_WARNINGS //add back??
  VTKM_EXEC
  void BlockLoad(const ThreadIndicesType& indices,
                 const ExecObjectType& execObject,
                 const ValueType (&buffer)[0])  //could this be const??? todo
  {
    // no-op for a non-specialized BlockFetch
  }

  /// \brief Store data from a work instance into the temporary buffer.
  ///
  /// All \c BlockFetch specializations are expected to have a constant method named
  /// \c Store that takes a \c ThreadIndices object containing thread-local
  /// indices, an execution object, and a value computed by the worklet call
  /// and stores that value into the given temporary buffer.
  /// If the store is not applicable (for example for a read-only
  /// fetch), this method can be a no-op.
  /// This base version of Store falls back to the Fetch::Store() method
  /// and stores the value directly into the execution object.
  ///
  VTKM_EXEC
  void Store(const ThreadIndicesType& indices,
             const ExecObjectType& execObject,
             const ValueType& value,
             const ValueType (&buffer)[0]) const
  {
    // pass through to a Fetch object
    Fetch<FetchTag, AspectTag, ThreadIndicesType, ExecObjectType> fetch;
    fetch.Store(indices, execObject, value);
  }

  /// \brief Store a block of data from the temporary buffer into the execution object.
  ///
  /// All \c BlockFetch specializations are expected to have a method named
  /// \c BlockStore that takes a \c ThreadIndices object containing thread-local
  /// indices, an execution object, and a temporary buffer and stores that
  /// the contents of the buffer into the execution object associated with this
  /// BlockFetch. If the store is not applicable (for example for a read-only
  /// fetch, or as in this base version), this method can be a no-op.
  ///
  //VTKM_SUPPRESS_EXEC_WARNINGS  //add back?? todo
  VTKM_EXEC
  void BlockStore(const ThreadIndicesType& indices,
                  const ExecObjectType& execObject,
                  const ValueType (&buffer)[0]) //can this be const todo
  {
    // no-op for a non-specialized BlockFetch
  }

  /// \brief Return the size of a block of data
  ///
  /// All \c BlockFetch specializations are expected to have a method named
  /// \c GetBufferSize that returns the number elements which should be allocated
  /// in the temporary buffer.  This base version does not use the buffer so
  /// a size of 0 is returned.
  VTKM_EXEC
  constexpr vtkm::Id GetBufferSize() const
  {
    // non-specialized BlockFetch does not use the buffer
    return 0;
  }
};

}
}
} // namespace vtkm::exec::arg

#endif //vtk_m_exec_arg_BlockFetch_h
